import React, { useContext, useState } from 'react'
import { GlobalContext } from '../App'
import ModalCanvas from './ModalCanvas'


function Product({ productDetails }) {

    const [productsData, onChangesSizes, filterSizesArr, filteredProductsData,
        cartItems, setCartItems, showCartBoard, setShowCartBoard] = useContext(GlobalContext)


    const {
        id,
        style,
        title,
        availableSizes,
        currencyFormat,
        currencyId,
        description,
        installments,
        isFreeShipping,
        price,
        sku,
        url,
        quantity
    } = productDetails

    const filterItems = cartItems.filter(each => each.id !== id)

    let filterCartItems = cartItems.map(each => {
        if (each.id == id) {
            return {
                ...each,
                quantity: each.quantity + 1
            }
        } else {
            return each
        }
    })

    const onAddToCart = () => {
        setCartItems(filterCartItems)
        setShowCartBoard(true)
    }

    return (
        <div className='d-flex flex-column m-4 shadow rounded-3 position-relative' style={{ width: '252.5px', height: '535px' }}>
            {
                isFreeShipping && <button type='button' className="btn btn-dark btn-sm position-absolute top-0 end-0"
                    style={{ width: "5.25rem", height: '1.5rem', fontSize: '0.66rem' }}>Free Shipping</button>
            }
            <img src={url} alt={style} />
            <div className='d-flex flex-column justify-content-center align-items-center mt-4 mb-2'>
                <h6>{title}</h6>
                <hr className='border border-warning w-25 mt-0' />
                <h6 className='mt-0'>${price}</h6>
                <p>or {installments} X $ {parseFloat(price / installments).toFixed(2)}</p>
            </div>
            <button type="button" className='btn btn-dark mt-1' onClick={onAddToCart}>Add to cart</button>
        </div>
    )
}

export default Product