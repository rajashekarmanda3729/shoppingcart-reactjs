import React, { useContext } from 'react'
import CartProduct from './CartProduct'
import { GlobalContext } from '../App'
import cartImage from '../assets/shopping-cart/static/bag-icon.png'


function CartItems() {

    const [productsData, onChangesSizes, filterSizesArr,
        filteredProductsData, cartItems, setCartItems] = useContext(GlobalContext)

    let itemsLength = 0
    cartItems.map(each => {
        if (each.quantity > 0) {
            itemsLength += each.quantity
        }
    })

    return (
        <div className='d-flex flex-column'>
            <h5 className='text text-warning text-center mt-3'>
                <img src={cartImage} alt="cart" />{itemsLength} <span className='text-light'>Cart</span>
            </h5>
            {
                cartItems && cartItems.map(eachItem => {
                    return (eachItem.quantity > 0) && <CartProduct key={eachItem.id} itemDetails={eachItem} />
                })
            }
        </div>
    )
}

export default CartItems