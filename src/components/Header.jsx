import React, { useContext } from 'react'
import cartImage from '../assets/shopping-cart/static/bag-icon.png'
import Cart from './Cart'
import bootstrap from 'bootstrap/dist/css/bootstrap.min.css'
import { GlobalContext } from '../App'

function Header() {

    return (
        <div className='d-flex flex-row justify-content-between bg-light'>
            <div>
                <img src='https://cdn.discordapp.com/attachments/1072061762536493137/1097450744652775515/githubicon.png'
                    alt='github' style={{ height: '3.5rem' }} />
            </div>
            <Cart />
        </div>

    )
}

export default Header