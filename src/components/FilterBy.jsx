import React, { useContext, useState } from 'react'
import Filter from './Filter'
import { v4 as uuidv4 } from 'uuid'
import { GlobalContext } from '../App'

function FilterBy() {

    const [productsData, addFilter, filterSizesArr] = useContext(GlobalContext)

    filterSizesArr.sort((a, b) => a.position > b.position ? 1 : -1)

    return (
        <div className='d-flex flex-column w-25'>
            <h3 className='ms-3'>Sizes:</h3>
            <div className='d-flex flex-row flex-wrap'>
                {
                    filterSizesArr && filterSizesArr.map(eachButton => {
                        return <Filter filterDetails={eachButton} key={eachButton.id} id={eachButton.id} />
                    })
                }
            </div>
            <div className='d-flex flex-column ms-3 mt-3'>
                <p>Leave a star on Github if this <br />repository was useful :{`)`}</p>
                <a href='https://gitlab.com/rajashekarmanda3729/shoppingcart-reactjs.git'>Start</a>
            </div>
        </div>
    )
}

export default FilterBy