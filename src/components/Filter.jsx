import React, { useState, useContext } from 'react'
import { GlobalContext } from '../App'

function Filter(props) {

    const { id, filterSizeName, isActive } = props.filterDetails

    const [productsData, onChangesSizes] = useContext(GlobalContext)

    const [activeButton, setActiveButton] = useState(false)

    const onChangeSelection = () => {
        onChangesSizes(filterSizeName)
        setActiveButton(prev => {
            console.log(!prev)
            return !prev
        })
    }

    return (
        <div className='d-flex '>
            <button className={`m-1 rounded-circle btn btn-${isActive ? 'dark' : 'light'}`} type='button'
                style={{ width: '4rem', height: '4rem' }} key={id}
                onClick={onChangeSelection}>{filterSizeName}</button>
        </div>
    )
}

export default Filter