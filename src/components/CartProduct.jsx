import React, { useContext } from 'react'
import { GlobalContext } from '../App'

function CartProduct({ itemDetails }) {

    const [productsData, onChangesSizes, filterSizesArr, 
        filteredProductsData, cartItems, setCartItems] = useContext(GlobalContext)

    const {
        id,
        style,
        title,
        availableSizes,
        currencyFormat,
        currencyId,
        description,
        installments,
        isFreeShipping,
        price,
        sku,
        url,
        quantity
    } = itemDetails

    const increaseItemQuantity = () => {
        let filterCartItems = cartItems.map(each => {
            if (each.id == id) {
                return {
                    ...each,
                    quantity: each.quantity + 1
                }
            } else {
                return each
            }
        })

        setCartItems(filterCartItems)
    }

    const decreaseItemQuantity = () => {
        let filterCartItems = cartItems.map(each => {
            if (each.id == id && each.quantity > 1) {
                return {
                    ...each,
                    quantity: each.quantity - 1
                }
            } else {
                return each
            }
        })

        setCartItems(filterCartItems)
    }

    const onRemoveItem = () => {
        let filterCartItems = cartItems.filter(each => {
            if (each.id != id) {
                return each
            }
        })

        setCartItems(filterCartItems)
    }

    return (
        <div className="card mb-3 bg bg-dark p-1" style={{ maxWidth: '540px', height: '7rem' }}>
            <div className="row g-0">
                <div className="col-md-2">
                    <img src={url} className="img-fluid rounded-start w-75" alt={title} />
                </div>
                <div className="col-md-8">
                    <div className="card-body">
                        <h5 className="card-title text-secondary">{title}</h5>
                        <p className="card-text text-secondary">{availableSizes && availableSizes[0]} | Q:{quantity}</p>
                    </div>
                </div>
                <div className="d-flex flex-column col-md-2 text-dark">
                    <button className='btn btn-secondary w-50 h-50 bg-transparent border border-0 text-secondary ms-5' onClick={onRemoveItem} >X</button>
                    <p className='mb-0 text-warning text-sm'>$ {price}</p>
                    <div className='mt-0'>
                        <button type='button' className='btn btn-outline-secondary btn-sm m-1' onClick={increaseItemQuantity}>+</button>
                        <button type='button' className='btn btn-outline-secondary btn-sm m-1' onClick={decreaseItemQuantity}>-</button>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default CartProduct