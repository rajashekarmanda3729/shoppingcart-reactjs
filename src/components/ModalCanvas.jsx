import React, { useContext, useState } from 'react'
import cartImage from '../assets/shopping-cart/static/bag-icon.png'
import CartItems from './CartItems'
import { GlobalContext } from '../App'


function ModalCanvas() {

    const [showButton, setShowButton] = useState(false)

    const [productsData, onChangesSizes, filterSizesArr,
        filteredProductsData, cartItems, setCartItems, showCartBoard, setShowCartBoard] = useContext(GlobalContext)


    let totalAmout = 0
    let itemsCount = 0
    cartItems.map(eachItem => {
        if (eachItem.quantity > 0) {
            totalAmout += eachItem.price * eachItem.quantity
            itemsCount += eachItem.quantity
        }
    })

    const checkOut = () => {
        alert(`Checkout - Subtotal: $ ${totalAmout}`)
    }


    const onShowCart = () => setShowCartBoard(prev => !prev)

    return (
        <div>
            <button
                className="btn btn-dark"
                type="button"
                style={{ width: showCartBoard ? '34rem' : '' }}
                onClick={onShowCart}
            >
                <h4 className='text-warning text-start'>
                    {!showCartBoard ? <img src={cartImage} alt="cart" /> : 'X'}{!showCartBoard ? itemsCount : ''}
                </h4>
            </button>
            <div
                className={`offcanvas offcanvas-end ${showCartBoard ? 'show' : ''} text bg-dark`}
                data-bs-scroll="true"
                data-bs-backdrop="false"
                tabIndex={-1}
                id="offcanvasScrolling"
                aria-labelledby="offcanvasScrollingLabel"
                style={{ width: '27%' }}
            >
                <div className="offcanvas-header">

                </div>
                <div className="offcanvas-body text text-light">
                    <CartItems />
                </div>
                <div className="shadow border-top border-secondary offcanvas-footer text text-light pb-3 bg-dark d-flex flex-column justify-content-center align-items-center">
                    <div className='d-flex flex-row justify-content-between p-3 w-100'>
                        <h5 style={{ color: 'gray' }}>Subtotal</h5>
                        <div className='d-flex flex-column'>
                            <h3 className='text-warning'>$ {totalAmout.toFixed(2)}</h3>
                            <h6 className='text-secondary'>OR UP TO 5 X {(totalAmout / 5).toFixed(2)}</h6>
                        </div>
                    </div>
                    <button type='button' className='btn btn-outline-secondary w-75' onClick={checkOut}>Checkout</button>
                </div>
            </div>
        </div >
    )
}

export default ModalCanvas