import React, { useState, useEffect, useReducer, createContext } from 'react'
import AllProducts from './AllProducts'
import FilterBy from './FilterBy'

function DisplayPage() {

  return (
    <div className='d-flex justify-content-center p-5'>
      <FilterBy />
      <AllProducts />
    </div>
  )
}

export default DisplayPage