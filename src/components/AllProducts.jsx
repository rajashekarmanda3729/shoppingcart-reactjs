import React, { createContext, useContext, useEffect, useState } from 'react'
import Product from './Product'
import { v4 as uuidv4 } from 'uuid'
import { GlobalContext } from '../App'

function AllProducts() {

    const [productsData, addFilter, filterSizesArr, filteredProductsData] = useContext(GlobalContext)

    const data = filteredProductsData.length !== 0 ? filteredProductsData : productsData

    return (
        <div className='d-flex flex-column justify-content-center w-75'>
            <h5 className='ms-5'>{filteredProductsData.length !== 0 ? filteredProductsData.length : productsData.length} Product(s) found </h5>
            <div className='d-flex flex-row justify-content-center flex-wrap'>
                {
                    data && data.map(eachProduct => {
                        return <Product key={uuidv4()} productDetails={eachProduct} />
                    })
                }
            </div>
        </div>
    )
}

export default AllProducts