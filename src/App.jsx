import React, { useState, useReducer, createContext, useEffect } from 'react'
import DisplayPage from './components/DisplayPage'
import Header from './components/Header'
import initialProductsData from '../src/assets/shopping-cart/data.json'
import { v4 as uuidv4 } from 'uuid'
import './App.css'

export const GlobalContext = createContext()


function App() {

  const filtersArr = [
    {
      id: uuidv4(),
      filterSizeName: 'XS',
      isActive: false,
      position: 0
    },
    {
      id: uuidv4(),
      filterSizeName: 'S',
      isActive: false,
      position: 1
    },
    {
      id: uuidv4(),
      filterSizeName: 'M',
      isActive: false,
      position: 2
    },
    {
      id: uuidv4(),
      filterSizeName: 'ML',
      isActive: false,
      position: 3
    },
    {
      id: uuidv4(),
      filterSizeName: 'L',
      isActive: false,
      position: 4
    },
    {
      id: uuidv4(),
      filterSizeName: 'XL',
      isActive: false,
      position: 5
    }, {
      id: uuidv4(),
      filterSizeName: 'XXL',
      isActive: false,
      position: 6
    },

  ]

  let data = initialProductsData.products.map(each => {
    return { ...each, quantity: 0 }
  })

  const [productsData, setProductsData] = useState(data)
  const [filterSizesArr, setFilterSizesArr] = useState(filtersArr)
  const [selected, setSelected] = useState([])
  const [cartItems, setCartItems] = useState(data)
  const [showCartBoard, setShowCartBoard] = useState(false)


  useEffect(() => {
    let filtersActiveSizes = []
    filterSizesArr.map(each => {
      if (each.isActive == true) filtersActiveSizes.push(each.filterSizeName)
    })

    setSelected(prev => {
      const filterArr = filtersActiveSizes.filter(each => !selected.includes(each))
      return [...filterArr, ...prev]
    })

  }, [filterSizesArr])

  let filteredProductsData = []

  selected.map(eachSize => {
    productsData.map(eachP => {
      if (!filteredProductsData.includes(eachP) && eachP.availableSizes.includes(eachSize)) {
        filteredProductsData.push(eachP)
      }
    })
  })

  const onChangesSizes = (filter) => {

    if (!selected.includes(filter)) {

      let currentSelectedSize
      filterSizesArr.filter(each => {
        if (each.filterSizeName == filter) {
          currentSelectedSize = each
        }
      })

      const restFilterSizes = filterSizesArr.filter(each => each.filterSizeName !== filter)
      const currentIs = filterSizesArr.filter(each => {
        return {
          ...each,
          isActive: !each.isActive
        }
      })
      setFilterSizesArr(prev => {
        return [{ ...currentSelectedSize, isActive: !currentSelectedSize.isActive }, ...restFilterSizes]
      })

    } else {

      const filterSizes = selected.filter(each => each != filter)
      setSelected(filterSizes)

      let currentSelectedSize
      filterSizesArr.filter(each => {
        if (each.filterSizeName == filter) {
          currentSelectedSize = each
        }
      })

      const restFilterSizes = filterSizesArr.filter(each => each.filterSizeName !== filter)
      const currentIs = filterSizesArr.filter(each => {
        return {
          ...each,
          isActive: !each.isActive
        }
      })
      setFilterSizesArr(prev => {
        return [...restFilterSizes, { ...currentSelectedSize, isActive: !currentSelectedSize.isActive }]
      })

    }
  }

  // console.log(showCartBoard)

  return (
    <div className="d-flex flex-column">
      <GlobalContext.Provider value={[productsData, onChangesSizes, filterSizesArr,
        filteredProductsData, cartItems, setCartItems, showCartBoard, setShowCartBoard]}>
        <Header />
        <DisplayPage />
      </GlobalContext.Provider>
    </div >
  )
}

export default App
